#include <stdio.h>
#include <omp.h>

int main(){
    printf("Iniciando a região de threads paralelas!\n");
    int x;
    printf("Digite o número de threads:");
    scanf("%d", &x);
    omp_set_num_threads(x); //Define o número de Threads

    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int nt = omp_get_num_threads();
        printf("Total de threads %d <==> Thread em execução numero %d\n", nt, id);
    }
    printf("Todas threads executadas! Finalizando a região de threads!\n");
    return 0;
}